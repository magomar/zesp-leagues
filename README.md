# ZESP-Leagues

Tool to keep track of ZESP leagues based on data from Zwift Power.

Install with `pip install .`

Execute with `zesp-league path/to/league_Description.yaml`. For example:

    zesp-league data/leagues/II_ZESP_League.yaml
 
Event results and league rankings are automatically saved to files. 
By default, the **csv** format is used, but it is also possible to 
use **excel** by including the `--excel` flag when running the app.

    zesp-league data/leagues/II_ZESP_League.yaml --excel
    
When generating the rankings, it is possible to set a score of zero 
to those events with no participation. This is done with the `--zeros` flag.

    zesp-league data/leagues/II_ZESP_League.yaml --zeros