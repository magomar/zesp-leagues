def cast_optional(raw_value, cast_func):
    return cast_func(raw_value) if raw_value else None
