from typing import Dict, List

RANKING_ES = {
    'pos': 'Pos.',
    'cat': 'Cat.',
    'cat_pos': 'Pos. cat.',
    'name': 'Nombre',
    'score': 'Total',
    'cat_score': 'Puntos cat.'
}
EVENT_ES = {
    'pos': 'Pos.',
    'cat': 'Cat.',
    'cat_pos': 'Pos. cat.',
    'name': 'Nombre',
    'score': 'Puntos',
    'cat_score': 'Puntos cat.'
}


def columns_mapper(columns: List[str], translator: Dict[str, str]):
    return {col: translator[col] for col in columns if col in translator}
