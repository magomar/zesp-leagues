import time
from datetime import datetime
from pathlib import Path
from typing import Any, Dict, Optional

import requests
from domain.event import EventResults
from domain.profile import Profile
from domain.team import TeamRider
from selenium import webdriver
from web_scraping import HEADERS
from zesp_leagues.domain.team import TeamHistory

API_VERSION = '3'
BASE_URL = 'https://www.zwiftpower.com/'
API_URL = f'{BASE_URL}/api{API_VERSION}.php?do'

session: Optional[requests.Session] = None
web_driver: Optional[webdriver.Firefox] = None


def get_session() -> requests.Session:
    global session
    return session or requests.Session()


def get_api_endpoint(command: str) -> str:
    return f'{API_URL}={command}'


def get_html_endpoint(command: str, suffix: str = '') -> str:
    return f'{BASE_URL}{command}.php?{suffix}'


def get_dynamic_html(url: str) -> str:
    driver = get_driver()
    driver.get(url)
    time.sleep(1)
    # execute script to scroll down the page
    # driver.execute_script(
    #     "window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    # time.sleep(2)
    # we get the internal html code of the body
    body = driver.execute_script("return document.body")
    return body.get_attribute('innerHTML')


def get_timestamp() -> float:
    return datetime.timestamp(datetime.now())


def api_call(command: str, params: Optional[Dict] = None) -> Dict:
    endpoint = get_api_endpoint(command)
    response = get_session().get(endpoint, params=params, headers=HEADERS)
    json = response.json()
    path = Path(__file__).parent.parent / 'temp' / f'{command}_{params}.json'
    open(path, 'w').write(str(response.json()))
    return json


def get_driver():
    global web_driver
    from selenium.webdriver.firefox.options import Options
    options = Options()
    options.add_argument("--headless")
    web_driver = webdriver.Firefox(options=options)
    return web_driver


# ************** MAIN **************


def event_list() -> Dict[str, str]:
    # params={'DAYS':} # in [1,7]
    # timestamp = get_timestamp()
    return api_call('zwift_event_list')


# ************** EVENTS **************

def event_results(event_id: int) -> EventResults:
    endpoint = get_html_endpoint('events')
    url = f'{endpoint}zid={event_id}#t_results'
    source = get_dynamic_html(url)
    return EventResults.create(source)


def event_results_staged(event_id: int) -> Dict[str, Any]:
    params = {'zid': event_id}
    return api_call('event_results_staged', params=params)


def event_primes(event_id: int) -> Dict[str, Any]:
    params = {'zid': event_id,
              'category': '',
              'prime_type': 'msec'}
    return api_call('event_primes', params=params)


def critical_power_event(event_id: int) -> Dict[str, Any]:
    return api_call('critical_power_event', params=dict(zwift_event_id=event_id))


# ************** TEAM **************

def team_riders(team_id: int):
    endpoint = get_api_endpoint('team_riders')
    response = get_session().get(endpoint, params={'id': f'{team_id}'}, headers=HEADERS)
    return response.json()


def team_results(team_id: int) -> Dict[str, Any]:
    endpoint = get_api_endpoint('team_results')
    response = get_session().get(endpoint, params={'id': team_id}, headers=HEADERS)
    return response.json()


def team_history(team_id: int) -> TeamHistory:
    endpoint = get_html_endpoint('team', suffix='#tab-history')
    # url = f'{endpoint}id={team_id}#tab-history'
    response = get_session().get(endpoint, params={'id': team_id}, headers=HEADERS)
    return TeamHistory.create(team_id, response.content)


# ************** RIDERS **************

def rider_profile_results(zwid: int) -> Dict[str, Any]:
    endpoint = get_api_endpoint('profile_results')
    response = get_session().get(endpoint, params={'z': zwid}, headers=HEADERS)
    return response.json()


def rider_profile(zwid: int) -> Profile:
    endpoint = get_html_endpoint('profile')
    url = f'{endpoint}z={zwid}'
    source = get_dynamic_html(url)
    return Profile.create(zwid, source)


# Example of api3 endpoints
# https://www.zwiftpower.com/api3.php?do=league_event_results&id=480
# https://www.zwiftpower.com/api3.php?do=event_results&zid=1077298
# https://zwiftpower.com/api3.php?do=zwift_rider_records&type=totalDistance&_=1559867418533
# https://zwiftpower.com/api3.php?do=zwift_event_list&_=1559867418532

# ZWID = 92730
# profile = profile(ZWID)
# pass
