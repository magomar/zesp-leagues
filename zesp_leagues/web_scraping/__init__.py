from environs import Env
from requests.auth import HTTPBasicAuth


def get_auth() -> HTTPBasicAuth:
    env = Env()
    env.read_env()
    return HTTPBasicAuth(env('USER'), env('PASSWORD'))


HEADERS = {'User-Agent': 'Mozilla/5.0'}