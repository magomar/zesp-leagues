from typing import Optional

import requests
from zesp_leagues.domain.route import ZwiftRoutes

API_VERSION = '3'
BASE_URL = 'https://zwiftinsider.com/'
HEADERS = {'User-Agent': 'Mozilla/5.0'}

session: Optional[requests.Session] = None


def get_session() -> requests.Session:
    global session
    return session or requests.Session()


def get_html_endpoint(command: str) -> str:
    return f'{BASE_URL}{command}/'


def routes():
    endpoint = get_html_endpoint('routes')
    response = get_session().get(endpoint, headers=HEADERS)
    return ZwiftRoutes.create(response.content)


