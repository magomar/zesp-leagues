import os
from pathlib import Path
from typing import Any, Dict, List

import pandas as pd
import yaml

import zesp_leagues.web_scraping.zwiftpower_api as api
from zesp_leagues import get_data_path
from zesp_leagues.translate import EVENT_ES, RANKING_ES, columns_mapper

CATEGORIES = ('A', 'B', 'C', 'D', 'E')
COLUMNS = ['cat_pos', 'name', 'zwid', 'time_gun', 'team', 'cat', 'hr', 'weight', 'height']
COLUMNS_TO_SAVE = ['pos', 'cat', 'cat_pos', 'name', 'zwid', 'time_gun', 'weight', 'height', 'score', 'cat_score']

RANKING_COLUMNS = ['pos', 'name', 'score']
EVENT_COLUMNS = ['pos', 'cat', 'cat_pos', 'name', 'score', 'cat_score']

ZWIFTPOWER_FOLDER = get_data_path('zwift_power')
LEAGUES_FOLDER = get_data_path('leagues')


def obtain_zp_data(zid: int, raw_data_path: Path) -> pd.DataFrame:
    zp_data_file = raw_data_path / f'event_results_{zid}.csv'
    if zp_data_file.exists():
        zp_data = pd.read_csv(zp_data_file)
    else:
        event_results = api.event_results(zid)
        zp_data = pd.DataFrame(event_results.results)
        zp_data.to_csv(zp_data_file, index=False)
    return zp_data


def export_data(data: pd.DataFrame, path: Path, filename_no_ext: str, excel=False):
    if excel:
        data.to_excel(f'{filename_no_ext}.xlsx', index=False)
    else:
        data.to_csv(f'{filename_no_ext}.csv', index=False)


class League(object):
    def __init__(self,
                 league_details: Dict[str, Any],
                 excel: bool = False,
                 zeros: bool = False,
                 event_columns: List[str] = EVENT_COLUMNS,
                 ranking_columns: List[str] = RANKING_COLUMNS):
        self.name = league_details['name']
        self.teams = league_details['teams']
        self.pos_scores = league_details['pos_scores']
        self.best_events = league_details['best_events']
        self.events = league_details['events']
        self.valid_events = [event for event in self.events if event not in league_details['non_scoring']]
        self.len = len(self.events)
        self.max_score_idx = len(self.pos_scores) - 1
        self.excel = excel
        self.zeros = zeros
        self.event_columns = event_columns
        self.ranking_columns = ranking_columns
        self.league_dir = LEAGUES_FOLDER / self.name
        self.zwiftpower_dir = ZWIFTPOWER_FOLDER / self.name
        self.ranking_dir = self.league_dir / 'Rankings'

    def generate_league_results(self):
        if not self.league_dir.exists():
            self.league_dir.mkdir()
        if not self.zwiftpower_dir.exists():
            self.zwiftpower_dir.mkdir()
        if not self.ranking_dir.exists():
            self.ranking_dir.mkdir()
        riders = {}
        for event in self.valid_events:
            event_results = StageResults(event, self)
            for record in event_results.data:
                zwid = record['zwid']
                if zwid not in riders:
                    riders[zwid] = RiderResults(zwid, record['name'])
                riders[zwid].add_results(record, event['stage'])
        for rider in riders.values():
            rider.aggregate_scores(self.best_events)

        summaries = [rider.summary for rider in riders.values()]

        # Global ranking
        rankings = sorted(summaries, key=lambda x: x['score'], reverse=True)
        data = pd.DataFrame.from_dict(rankings)
        data['pos'] = data.index + 1
        stage_columns = [f'E{event["stage"]}' for event in self.valid_events]
        if self.zeros:
            for col in stage_columns:
                data[col] = 0
        for summary in rankings:
            for stage, score in summary['stages'].items():
                data.loc[data.zwid == summary['zwid'], f'E{stage}'] = score

        columns = self.ranking_columns[:-1] + stage_columns + [
            self.ranking_columns[-1]]
        data = data.reindex(columns=columns)
        data.rename(columns=columns_mapper(data.columns, RANKING_ES), inplace=True)
        export_data(data, self.ranking_dir / f'Ranking_{self.len}', excel=self.excel)

        # Category rankings
        for cat in CATEGORIES:
            rankings = sorted(summaries, key=lambda x: x[cat], reverse=True)
            data = pd.DataFrame.from_dict(rankings)
            data = data.loc[data[cat] > 0]
            data.reset_index(drop=True, inplace=True)
            data['pos'] = data.index + 1
            # data['cat'] = cat
            data.drop(columns='score', inplace=True)
            data.rename(columns={cat: 'score'}, inplace=True)
            if self.zeros:
                for col in stage_columns:
                    data[col] = 0
            for summary in summaries:
                for stage, score in summary[f'{cat}_stages'].items():
                    data.loc[data.zwid == summary['zwid'], f'E{stage}'] = score
            data = data.reindex(columns=columns)
            data.rename(columns=columns_mapper(data.columns, RANKING_ES), inplace=True)
            export_data(data, self.ranking_dir, f'{cat}_ranking_{self.len}', excel=self.excel)

    def obtain_score(self, pos: int):
        return self.pos_scores[min(pos - 1, self.max_score_idx)]


class StageResults:

    def __init__(self,
                 event: Dict[str, Any],
                 league: League):
        self.league = league
        self.stage = event['stage']
        self.zid = event['zid']
        self.name = event['name']
        self.excluded = event['excluded']
        self.stage_dir = os.path.join(self.league.league_dir, f'Stage_{self.stage}')
        if not os.path.exists(self.stage_dir):
            os.mkdir(self.stage_dir)

        # data_file = os.path.join(self.league.events_dir, f'{self.name}_{self.zid}.csv')
        # if os.path.exists(data_file):
        #     self.data = pd.read_csv(data_file)
        #     self.info = self.load_info()
        # else:
        zp_data = obtain_zp_data(self.zid, self.league.zwiftpower_dir)
        self.generate_event_results(zp_data)

    def generate_event_results(self, zp_data: pd.DataFrame):
        data = zp_data[COLUMNS]

        # Remove non ZESP Team members
        non_allowed = data[~data.team.isin(self.league.teams)]
        data.drop(non_allowed.index, inplace=True)

        # Remove participants without HRM
        no_hr = data[pd.isna(data.hr)]
        data.drop(no_hr.index, inplace=True)

        # Remove participants with non reliable power source
        # bad_power_source = data[data.power_type != 3]
        # data.drop(bad_power_source.index, inplace=True)
        other_excluded = data[data.zwid.isin(self.excluded)]
        data.drop(other_excluded.index)

        # Sort participants by time of arrival
        data = data.sort_values(by=['time_gun'])
        data.reset_index(drop=True, inplace=True)

        cat_pos = {}
        for cat in CATEGORIES:
            cat_pos[cat] = 1

        for index, row in data.iterrows():
            cat = row['cat']
            pos = index + 1
            data.loc[index, 'pos'] = pos
            data.loc[index, 'cat_pos'] = cat_pos[cat]
            data.loc[index, 'score'] = self.league.obtain_score(pos)
            data.loc[index, 'cat_score'] = self.league.obtain_score(cat_pos[cat])
            cat_pos[cat] += 1
        data = data.reindex(columns=COLUMNS_TO_SAVE)
        self.data = data.to_dict('records')

        # Save event results
        report_data = data[self.league.event_columns]
        report_data.rename(columns=columns_mapper(report_data.columns, EVENT_ES), inplace=True)
        export_data(report_data, self.stage_dir, f'Stage_{self.stage}-Results', excel=self.league.excel)

        # Save event info
        omitted = {'Non ZESP': non_allowed,
                   'No HRM': no_hr,
                   'Other': other_excluded,
                   # 'No Powermeter': no_powermeter
                   }
        self.save_info(omitted)

    def load_info(self):
        info_file = os.path.join(self.stage_dir, f'Stage_{self.stage}-Omitted.yaml')
        with open(info_file) as stream:
            try:
                stage_info = yaml.safe_load(stream)
                return stage_info
            except yaml.YAMLError as error:
                print(f'Error loading {info_file}')
                print(error)

    def save_info(self, omitted: Dict[str, pd.DataFrame]):
        info = {}
        for key, data_frame in omitted.items():
            info[key] = [f'{row["name"]}({row.zwid})' for i, row in data_frame.iterrows()]

        info_file = os.path.join(self.stage_dir, f'Stage_{self.stage}-Omitted.yaml')
        with open(info_file, 'w') as writer:
            yaml.dump(info, writer, default_flow_style=False)


class RiderResults:

    def __init__(self, zwid: int, name: str):
        self.zwid = zwid
        self.name = name
        self.scores = {}
        self.agg_score = 0
        self.cat_scores = {}
        self.agg_cat_score = {}
        for cat in CATEGORIES:
            self.cat_scores[cat] = {}
            self.agg_cat_score[cat] = 0

    def add_results(self, record: Dict[str, Any], stage: int):
        self.scores[stage] = record['score']
        self.cat_scores[record['cat']][stage] = record['cat_score']

    def aggregate_scores(self, best_events: int):
        scores = sorted(self.scores.values(), reverse=True)
        self.agg_score = sum(scores[:min(best_events, len(scores))])
        for cat in CATEGORIES:
            cat_scores = sorted(self.cat_scores[cat].values(), reverse=True)
            self.agg_cat_score[cat] = sum(cat_scores[:min(best_events, len(cat_scores))])

    @property
    def summary(self):
        summary = {
            'zwid': self.zwid,
            'name': self.name,
            'score': self.agg_score,
            'stages': self.scores

        }
        for cat in CATEGORIES:
            summary[cat] = self.agg_cat_score[cat]
            summary[f'{cat}_stages'] = self.cat_scores[cat]
        return summary
