import html
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import List

import bs4
import pandas as pd
from bs4 import BeautifulSoup, Tag

TROPHY_COLOR_TO_POS = {
    '#FDD017': 1,
    '#C0C0C0': 2,
    '#CD7F32': 3
}


def get_pos_from_trophy(tag: Tag) -> int:
    color = tag.i['style'].split(':')[1]
    return TROPHY_COLOR_TO_POS[color]


def get_timedelta(time_fields: List[str]) -> timedelta:
    if len(time_fields) == 1:
        return timedelta(seconds=0)
    time_gun_raw = time_fields[1].strip()
    if time_gun_raw[-1] == 's':
        return timedelta(seconds=float(time_gun_raw[:-1]))
    else:
        time_gun_fields = time_gun_raw.split(':')
        return timedelta(minutes=int(time_gun_fields[0]), seconds=int(time_gun_fields[1]))


@dataclass
class RiderEventResults:
    pos: int
    cat: str
    cat_pos: int
    name: str
    zwid: int
    team: str
    time: float
    time_gun: float
    avg: float
    wats: int
    np: int
    p20m: float
    p5m: float
    p1m: float
    p30s: float
    p15s: float
    weight: float
    hr: int
    hr_max: int
    height: int
    gain: float

    @classmethod
    def create(cls, row: bs4.element.Tag, pos: int):
        table_cells = row.find_all('td')
        fields = [x.text for x in table_cells]
        cat = fields[0].strip()
        cat_pos = int(fields[1]) if fields[1] else get_pos_from_trophy(table_cells[1])
        rider_info = table_cells[2].find_all('a')
        name = html.unescape(rider_info[0].text.strip())
        zwid = rider_info[0]['href'].split('=')[1]
        team = rider_info[1].text if len(rider_info) > 1 else None
        team_id = rider_info[1]['href'].split('=')[1] if team else None
        time_fields = fields[3].split('+')
        raw_time = time_fields[0].strip()
        time_format = '%M:%S' if len(raw_time) < 6 else '%H:%M:%S'
        t = datetime.strptime(raw_time, time_format).time()
        time = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second).total_seconds()
        time_gun = get_timedelta(time_fields).total_seconds()
        avg = float(fields[4][:-4]) if fields[4] else None
        wats = int(fields[5][:-1]) if fields[5] else None
        np = int(fields[6][:-1]) if fields[6] else None
        p20m = float(fields[7][:-4]) if fields[7] else None
        p5m = float(fields[8][:-4]) if fields[8] else None
        p1m = float(fields[9][:-4]) if fields[9] else None
        p30s = float(fields[10][:-4]) if fields[10] else None
        p15s = float(fields[11][:-4]) if fields[11] else None
        weight = float(fields[12][:-2]) if fields[12] else None
        bpm = int(fields[13][:-3]) if fields[13] else None
        bpm_max = int(fields[14][:-3]) if fields[14] else None
        height = int(fields[15][:-2]) if fields[15] else None
        gain = float(fields[16]) if fields[16] else None
        return cls(pos, cat, cat_pos, name, zwid, team, time, time_gun, avg, wats, np, p20m, p5m, p1m, p30s, p15s,
                   weight, bpm, bpm_max, height, gain)


@dataclass
class EventResults:
    results: List[RiderEventResults]

    @classmethod
    def create(cls, content: str):
        soup = BeautifulSoup(content, 'html.parser')
        rows = soup.tbody.find_all('tr')
        results = [RiderEventResults.create(row, i + 1) for i, row in enumerate(rows)]
        return cls(results)

    def to_dataframe(self):
        return pd.DataFrame(self.results)
