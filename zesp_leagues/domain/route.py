from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Iterable, List

import bs4
import pandas as pd
from bs4 import BeautifulSoup

from zesp_leagues import get_data_path


@dataclass
class Route:
    name: str
    world: str
    length: float
    elevation: int
    lead_in: float
    xp: int
    restrictions: List[str]
    url: str

    @classmethod
    def create(cls, row: bs4.element.Tag):
        table_cells = row.find_all('td')
        url = table_cells[0].a['href']
        fields = (x.text for x in table_cells)
        name, world, length, elevation, lead_in, xp, restrictions = fields
        restrictions = restrictions.split(',')
        length = float(length.split()[0])
        elevation = int(elevation.split()[0]) if elevation else -1
        lead_in = float(lead_in.split()[0])
        xp = int(xp.split()[0]) if xp else 0
        return cls(name, world, length, elevation, lead_in, xp, restrictions, url)


def partition(iterable: Iterable, pred: Callable):
    trues, falses = [], []
    for item in iterable:
        if pred(item):
            trues.append(item)
        else:
            falses.append(item)
    return trues, falses


@dataclass
class ZwiftRoutes:
    routes: List[Route]
    trails: List[Route]

    @classmethod
    def create(cls, content: bytes):
        soup = BeautifulSoup(content, 'html.parser')
        rows = soup.tbody.find_all('tr')
        all_routes = (Route.create(row) for row in rows)
        trails, routes = partition(all_routes, lambda x: 'Run Only' in x.restrictions)
        return cls(routes, trails)

    def to_csv(self):
        folder = get_data_path('routes')
        pd.DataFrame(self.routes).to_csv(folder / 'routes.csv', index=False)
        pd.DataFrame(self.trails).to_csv(folder / 'trails.csv', index=False)

    @classmethod
    def from_csv(cls):
        folder = get_data_path('routes')
        routes_df = pd.read_csv(folder / 'routes.csv')
        trails_df = pd.read_csv(folder / 'trails.csv')
        routes = [
            (Route(row.name, row.world, row.length, row.elevation, row.lead_in, row.xp, row.restrictions, row.url)) for
            index, row in routes_df.iterrows()]
        trails = [
            (Route(row.name, row.world, row.length, row.elevation, row.lead_in, row.xp, row.restrictions, row.url)) for
            index, row in trails_df.iterrows()]
        return cls(routes, trails)
