from dataclasses import dataclass, field

from bs4 import BeautifulSoup

# MATCH_ALL = r'.*'
#
#
# def like(string):
#     string_ = string
#     if not isinstance(string_, str):
#         string_ = str(string_)
#     regex = MATCH_ALL + re.escape(string_) + MATCH_ALL
#     return re.compile(regex, flags=re.DOTALL)
#
#
# def find_by_text(soup, text, tag, **kwargs):
#     elements = soup.find_all(tag, **kwargs)
#     matches = []
#     for element in elements:
#         if element.find(text=like(text)):
#             matches.append(element)
#     if len(matches) > 1:
#         raise ValueError('Too many matches:\n' + '\n'.join(matches))
#     elif len(matches) == 0:
#         return None


@dataclass
class Profile:
    zwid: int
    min_category: str
    min_category_female: str
    club: str
    ftp: int
    weight: float
    ftp_wkg: float = field(init=False)

    def __post_init__(self):
        self.ftp_wkg = self.ftp / self.weight if self.ftp and self.weight else -1

    @classmethod
    def create(cls, zwid: int, content: str):
        soup = BeautifulSoup(content, 'html.parser')
        profile = {}
        # aurl = soup.find('img', {'class': 'img-circle'})
        # if aurl:
        #     avatar_url = soup.find('img', {'class': 'img-circle'}).get('src')
        # else:
        #     avatar_url = None
        long_bio=soup.find(id='long_bio')
        zwift_level = soup.find(title='Zwift in-game level')
        profile['long_bio'] = long_bio.text if long_bio else None
        profile['zwift_level'] = zwift_level.text if zwift_level else None
        table = soup.find('table', id='profile_information')
        if table:
            table_rows = table.tbody.find_all('tr')
            for tr in table_rows:
                td = tr.find_all(['td', 'th'])
                row = [i.text.strip() for i in td]
                # if 'Country' in row and not row[1][0].isdigit():
                #     profile['country'] = row[1] or None
                # if 'Race Ranking' in row:
                #     profile['race_rank_pts'] = int(''.join(c for c in row[1].split(' ')[0] if c.isdigit()))
                #     profile['race_rank_place'] = int(
                #         ''.join(c for c in row[1].split(' pts in ')[1] if c.isdigit()))
                # if 'Category' in row:
                #     profile['race_rank_catagory'] = int(''.join(c for c in row[1] if c.isdigit()))
                # if 'Age Group' in row:
                #     profile['race_rank_age'] = int(''.join(c for c in row[1] if c.isdigit()))
                # if 'Weight Group' in row:
                #     profile['race_rank_weight'] = int(''.join(c for c in row[1] if c.isdigit()))
                if 'Team' in row:
                    row_test = ''.join(c for c in row[1] if c.isdigit())
                    if row_test.isdigit():
                        profile['race_rank_team'] = int(''.join(c for c in row[1] if c.isdigit()))
                    else:
                        profile['race_team'] = row[1] or None
                if 'Minimum Category' in row:
                    profile['minimum_category'] = row[1][0] or None
                    profile['minumun_catagory_female'] = row[1][2] if row[1][2] in ['A', 'B', 'C', 'D',
                                                                                    'E'] else None
                    profile['races'] = int(''.join(c for c in row[1] if c.isdigit()))
                # if 'Age' in row:
                #     profile['age'] = row[1] or None
                # if 'Average' in row:
                #     profile['average_watts'] = int(row[1].split('watts')[0])
                #     profile['average_wkg'] = float(row[1].split(' / ')[1].replace('wkg', ''))
                if 'FTP' in row:
                    try:
                        profile['ftp'] = int(row[1].split('w')[0])
                        profile['kg'] = int(''.join(c for c in row[1].split('~ ')[1] if c.isdigit()))
                    except:
                        profile['ftp'] = None
                        profile['kg'] = None
        min_category = profile.get('minimum_category', None)
        min_category_female = profile.get('minimum_category', None)
        club = profile.get('race_team', None)
        ftp = profile.get('ftp', None)
        weight = profile.get('kg', None)
        return cls(zwid, min_category, min_category_female, club, ftp, weight)
