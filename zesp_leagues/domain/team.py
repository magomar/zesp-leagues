from dataclasses import dataclass, field
from datetime import datetime, timedelta
from typing import Any, Dict, List, Optional, Tuple

import pandas as pd
from bs4 import BeautifulSoup

from util import cast_optional
from zesp_leagues import get_data_path

CATEGORY_MAPPING = {40: 'D',
                    30: 'C',
                    20: 'B',
                    10: 'A',
                    5: 'A+',
                    0: 'E'}


@dataclass
class Participation:
    when: datetime
    rider: str
    date: datetime.date = field(init=False)
    time: datetime.time = field(init=False)
    weekday: int = field(init=False)

    def __post_init__(self):
        self.date = self.when.date()
        self.time = self.when.time()
        self.weekday = self.when.weekday()

    @classmethod
    def create(cls, date: str, time: str, rider: str) -> 'Participation':
        date_time = datetime.combine(
            datetime.strptime(date, '%d-%m-%Y').date(),
            datetime.strptime(time, '%H:%M').time()
        ) + timedelta(hours=1)
        return cls(date_time, rider or '?')

    def as_tuple(self) -> Tuple[datetime.date, datetime.time, str]:
        return self.when, self.weekday, self.rider


@dataclass
class TeamHistory:
    team_id: int
    history: List[Participation]

    @classmethod
    def create(cls, team_id: int, content: bytes) -> 'TeamHistory':
        soup = BeautifulSoup(content, 'html.parser')
        rows = soup.tbody.find_all('tr')
        history = []  # type: List[Participation]
        for row in rows:
            fields = row.find_all('td')
            weekday, date, time = [x.text for x in fields[:3]]
            riders = [x.text for x in fields[3].find_all('a')]
            history.extend([Participation.create(date, time, rider) for rider in riders])
        return cls(team_id, history)

    def to_dataframe(self):
        return pd.DataFrame(self.history)


@dataclass
class TeamRider:
    div: str
    divw: str
    flag: str
    races: int
    ftp: int
    name: str
    aid: Optional[int]
    age: int
    zada: int
    w: float
    rank: Optional[float]
    skill: int
    distance: int
    climbed: int
    energy: int
    time: int
    skill_race: int
    skill_seg: int
    skill_power: int
    zwid: int
    status: Optional[str]
    reg: int
    email: Optional[str]
    h_1200_watts: Optional[int]
    h_1200_wkg: Optional[float]
    h_15_watts: Optional[int]
    h_15_wkg: Optional[float]

    @classmethod
    def create(cls, data: Dict[str, Any]):
        # div = Category(data['div']).name
        # divw = Category(data['divw']).name
        div = CATEGORY_MAPPING[data['div']]
        divw = CATEGORY_MAPPING[data['divw']]
        flag = data['flag']
        races = cast_optional(data['r'], int)
        ftp = cast_optional(data['ftp'][0], int)
        name = data['name']
        aid = cast_optional(data['aid'], int)
        age = data['age']
        zada = data['zada']
        weight = float(data['w'][0])
        rank = cast_optional(data['rank'], float)
        skill = data['skill']
        distance = data['distance']
        climbed = data['climbed']
        energy = data['energy']
        time = data['time']
        skill_race = data['skill_race']
        skill_seg = data['skill_seg']
        skill_power = data['skill_power']
        zwid = data['zwid']
        status = data['status'] or None
        reg = data['reg']
        email = data['email'] or None
        has_power_data = 'h_1200_watts' in data
        h_1200_watts = int(data['h_1200_watts'].replace('.', '')) if has_power_data else None
        h_1200_wkg = float(data['h_1200_wkg']) if has_power_data else None
        h_15_watts = int(float(data['h_15_watts'].replace(',', '').replace('.', ''))) if has_power_data else None
        h_15_wkg = float(data['h_15_wkg']) if has_power_data else None

        return cls(div, divw, flag, races, ftp, name, aid, age, zada, weight, rank, skill, distance, climbed, energy,
                   time, skill_race, skill_seg, skill_power, zwid, status, reg, email, h_1200_watts, h_1200_wkg,
                   h_15_watts, h_15_wkg)


@dataclass
class TeamRiders:
    team_id: int
    riders: List[TeamRider]

    @classmethod
    def create(cls, team_id: int, json_data: List[Dict[str, Any]]) -> 'TeamRiders':
        return cls(team_id, [TeamRider.create(rider_data) for rider_data in json_data])

    def to_dataframe(self):
        return pd.DataFrame(self.riders)

    def to_csv(self):
        pd.DataFrame(self.riders).to_csv(get_data_path('teams') / f'team_{self.team_id}.csv', index=False)

    @classmethod
    def from_csv(cls, team_id: int):
        riders_df = pd.read_csv(get_data_path('teams') / f'team_{team_id}.csv')
        riders = [TeamRider(*series) for i, series in riders_df.iterrows]
        return cls(team_id, riders)
