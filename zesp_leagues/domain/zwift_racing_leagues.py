from dataclasses import dataclass, field
from functools import cmp_to_key
from typing import Any, Dict, List, Sequence

import pandas as pd
import yaml
from tqdm import tqdm

from zesp_leagues import get_data_path
from zesp_leagues.web_scraping.zwiftpower_api import rider_profile

CAT_VALUE = dict(E=0, A=10, B=20, C=30, D=40)


@dataclass
class Rider:
    zwid: int
    name: str
    club: str
    cat: str
    ftp_wkg: float
    ftp: int
    weight: float
    disp: str
    status: str
    cat_value: int = field(init=False)

    def __post_init__(self):
        try:
            self.cat_value = CAT_VALUE[self.cat]
        except KeyError:
            self.cat_value = 100
            self.cat = 'X'


@dataclass
class ZRLTeam:
    name: str
    trc: int
    league: str
    gender: str
    division: str
    captain: int
    riders: List[Rider] = field(init=False)

    @classmethod
    def from_yaml(cls, yaml: Dict[str, Any]):
        name = yaml['name']
        trc = yaml['trc']
        league = yaml['league']
        gender = yaml['gender']
        division = yaml['division']
        captain = yaml['captain']
        return cls(name, trc, league, gender, division, captain)


@dataclass
class Club:
    name: str
    id: int
    manager: int
    pool: List[int] = field(init=False)

    @classmethod
    def from_yaml(cls, description: Dict[str, Any]):
        name = description['name']
        id = description['id']
        manager = description['manager']
        return cls(name, id, manager)


@dataclass
class ZRL:
    season: int
    club: Club
    teams: List[ZRLTeam]

    @classmethod
    def from_yaml(cls, description: Dict[str, Any]):
        season = description['season']
        club = Club.from_yaml(description['club'])
        teams = [ZRLTeam.from_yaml(team) for team in description['teams']]
        return cls(season, club, teams)


def load_zrl(filename: str):
    with open(get_data_path('zrl', filename)) as file:
        description = yaml.load(file, Loader=yaml.FullLoader)
    return ZRL.from_yaml(description)


def generate_pool(inscriptions: str, pool: str, reload: bool = False):
    pool_path = get_data_path('zrl', pool)
    if reload or not pool_path.exists():
        riders = []
        zwids = []
    else:
        pool = pd.read_csv(pool_path)

        riders = [Rider(*row.values) for i, row in pool.iterrows()]
        zwids = [rider.zwid for rider in riders]

    reg = pd.read_csv(get_data_path('zrl', inscriptions), names=['rider', 'zwid', 'cat', 'captain', 'disp', 'status'])
    for i, row in tqdm(reg.iterrows(), total=reg.shape[0]):
        zwid = row.zwid
        if zwid not in zwids:
            profile = rider_profile(zwid)
            cat = profile.min_category
            rider = Rider(zwid, row.rider, profile.club, cat, profile.ftp_wkg, profile.ftp,
                          profile.weight, row.disp, row.status)
            riders.append(rider)
    df = pd.DataFrame(riders).drop(columns='cat_value')
    df.sort_values(by=['cat', 'ftp_wkg'], ascending=[True, False], inplace=True)
    df.to_csv(get_data_path('zrl', 'zesp_pool.csv'), index=False)
    return riders


VALID_CAT = {'A', 'B', 'C', 'D'}


def _compare_riders(rider1: Rider, rider2: Rider):
    cat_cmp = rider2.cat_value - rider1.cat_value
    if cat_cmp != 0:
        return cat_cmp
    return rider1.ftp_wkg - rider2.ftp_wkg


compare_riders = cmp_to_key(_compare_riders)


def populate_teams(teams: Sequence[ZRLTeam], all_riders: Sequence[Rider], max_size=6, min_size=3) -> List[ZRLTeam]:
    riders_by_id = {rider.zwid: rider for rider in all_riders if rider.status.strip() == 'On'}
    for team in teams:
        captain = riders_by_id.pop(team.captain, None)
        team.riders = [captain] if captain else []
    riders = sorted(riders_by_id.values(), key=compare_riders, reverse=True)
    for team in teams:
        team_value = CAT_VALUE[team.division]
        while riders and len(team.riders) < max_size:
            if riders[0].cat_value >= team_value:
                team.riders.append(riders.pop(0))
            else:
                break
    return teams


def export_teams(teams: Sequence[ZRLTeam], season: int, stage: int):
    path = get_data_path('zrl', f'season_{season}_stage_{stage}.csv')
    df = pd.DataFrame(columns=['league', 'division', 'team', 'rider', 'cat'])
    for team in teams:
        for rider in team.riders:
            data = dict(league=team.league,
                        division=team.division,
                        team=team.name,
                        rider=rider.name,
                        cat=rider.cat)
            df = df.append(pd.Series(data, name=rider.zwid))
    df.to_csv(path, index=False)


riders = generate_pool('registered.csv', 'zesp_pool.csv', reload=False)
zrl = load_zrl('ZESP_ZRL_Season1.yaml')
teams = populate_teams(zrl.teams, riders)
export_teams(teams, 1, 1)
pass
