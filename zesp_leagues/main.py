import logging
from pathlib import Path

import typer
import yaml

from zesp_leagues import __version__
from zesp_leagues.league_results import League

app = typer.Typer()
state = {"verbose": False}

_logger = logging.getLogger(__name__)


def version_callback(value: bool):
    if value:
        typer.echo(f"Emolab Version: {__version__}")
        raise typer.Exit()


@app.callback()
def main(
        version: bool = typer.Option(False, '--version', callback=version_callback, is_eager=True),
        verbose: bool = typer.Option(False, '--verbose', '-v', help='increase output verbosity')
):
    logging.basicConfig(filename='zesp-leagues.log', level=logging.DEBUG, format='%(asctime)s %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    if verbose:
        typer.echo("Will write verbose output")
        state["verbose"] = True
    typer.echo(f'Running version {__version__}')


@app.command()
def update(
        league_description: Path = typer.Argument(..., exists=True,
                                                  help='Path to yaml file describing league events and rules'),
        excel: bool = typer.Option(False, '--excel', '-e',
                                   help='Whether to use excel instead of csv to store results'),
        zeros: bool = typer.Option(True, '--zeros', '-z', help='Whether to fill empty results with zeros')
):
    with open(league_description) as stream:
        try:
            league_details = yaml.safe_load(stream)
            league = League(league_details, excel=excel, zeros=zeros)
            league.generate_league_results()
            typer.echo(f'League results updated and saved to {league.league_dir}')
        except yaml.YAMLError as error:
            typer.echo(f'Error loading {league_details}')
            typer.echo(error)


if __name__ == "__main__":
    app()
