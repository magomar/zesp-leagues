from importlib.metadata import version
from pathlib import Path

__version__ = version('zesp-leagues')

_ROOT = Path(__file__).parent.parent


def get_data_path(*relative_path: str) -> Path:
    full_path = _ROOT / 'data'
    for sub_path in relative_path:
        full_path = full_path / sub_path
    return full_path
