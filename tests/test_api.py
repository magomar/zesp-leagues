from domain.event import EventResults
from pytest import fixture

from zesp_leagues.domain.team import Participation, TeamHistory
from zesp_leagues.web_scraping.zwiftpower_api import event_results, rider_profile_results, team_history, team_results, \
    team_riders


@fixture
def team_id():
    return 2146


@fixture
def zwift_id():
    return 92730


@fixture
def event_id():
    return 1077298


def test_event_results(event_id):
    results = event_results(event_id)
    assert isinstance(results, EventResults)


def test_team_riders(team_id):
    result = team_riders(team_id)
    assert isinstance(result, dict)
    assert 'data' in result


def test_team_results(team_id):
    result = team_results(team_id)
    assert isinstance(result, dict)
    assert 'events' in result
    assert 'data' in result


def test_team_history(team_id):
    history = team_history(team_id)
    assert isinstance(history, TeamHistory)
    assert isinstance(history.history[0], Participation)


def test_rider_profile(zwift_id):
    result = rider_profile_results(zwift_id)
    assert isinstance(result, dict)
    assert 'data' in result
